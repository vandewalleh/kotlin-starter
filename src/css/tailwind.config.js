module.exports = {
    purge: {
        content: [
            '../main/resources/views/**/*.twig',
            '../main/kotlin/starter/PebbleModule.kt',
        ]
    },
    theme: {},
    variants: {
        textColor: ['responsive', 'hover', 'focus', 'group-hover'],
    },
    plugins: [
    ],
    future: {
        removeDeprecatedGapUtilities: true,
        purgeLayersByDefault: true,
    },
}
