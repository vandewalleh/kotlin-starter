package starter.pebble

import com.mitchellbosecke.pebble.template.EvaluationContext
import com.mitchellbosecke.pebble.template.PebbleTemplate
import starter.Feature
import starter.utils.PebbleFilter

class HasFeatureFilter : PebbleFilter {
    override val name = "hasFeature"

    override fun getArgumentNames() = listOf("name")

    override fun apply(
        input: Any,
        args: MutableMap<String, Any>,
        self: PebbleTemplate,
        context: EvaluationContext,
        lineNumber: Int,
    ): Boolean {
        @Suppress("UNCHECKED_CAST")
        val features = input as List<Feature>

        val name = args["name"] as String

        return features.any { it.name == name }
    }
}
