package starter.pebble

import com.mitchellbosecke.pebble.extension.escaper.SafeString
import com.mitchellbosecke.pebble.template.EvaluationContext
import com.mitchellbosecke.pebble.template.PebbleTemplate
import org.intellij.lang.annotations.Language
import starter.Dependency
import starter.utils.PebbleFunction

// We need a custom function since pebble inserts whitespace everywhere
class DepAsXmlPebbleFunction : PebbleFunction {
    override val name = "depAsXml"
    override fun getArgumentNames() = listOf("dependency")

    override fun execute(
        args: Map<String, Any>,
        self: PebbleTemplate,
        context: EvaluationContext,
        lineNumber: Int,
    ): SafeString {
        val dep = args["dependency"] as Dependency

        fun tagName(name: String) = """<span class="text-red-700">$name</span>"""

        fun startTag(name: String): String {
            @Language("html") @Suppress("UnnecessaryVariable")
            val result = """<span class="text-gray-700">&lt;</span>""" +
                """${tagName(name)}<span class="text-gray-700">&gt;</span>"""
            return result
        }

        fun endTag(name: String): String {
            @Language("html") @Suppress("UnnecessaryVariable")
            val result = """<span class="text-gray-700">&lt;/</span>""" +
                """${tagName(name)}<span class="text-gray-700">&gt;</span>"""
            return result
        }

        fun tag(name: String, content: String) = """${startTag(name)}$content${endTag(name)}"""

        val result = """
            |${startTag("dependency")}
            |   ${tag("groupId", dep.groupId)}
            |   ${tag("artifactId", dep.artifactId)}
            |   ${tag("version", dep.version.value)}
            |${endTag("dependency")}
        """.trimMargin()

        return SafeString(result)
    }
}
