package starter.config

import com.electronwill.nightconfig.core.UnmodifiableConfig
import starter.*
import com.electronwill.nightconfig.core.Config as NightConfig

data class StarterConfig(
    val dependencies: List<Dependency>,
    val inputs: List<Input>,
    val features: List<Feature>,
)

class Config(private val cfg: NightConfig) {

    @Suppress("UNCHECKED_CAST")
    private fun NightConfig.configMap(key: String) = this.get<NightConfig>(key)
        ?.valueMap() as Map<String, NightConfig>?
        ?: emptyMap()

    fun load(): StarterConfig {

        @Suppress("UNCHECKED_CAST")
        val versions = cfg.get<UnmodifiableConfig>("versions")
            ?.valueMap() as Map<String, String>?
            ?: emptyMap()

        val repositories = cfg.configMap("repositories")
            .map { (name, values) ->
                Repository(name, values["url"])
            }

        val dependencies = cfg.configMap("dependencies")
            .map { (name, values) ->
                val versionKey: String = values["version"] ?: name
                val version = versions[versionKey] ?: error("Missing version for $name")

                val repositoryName: String? = values["repository"]
                val repo = repositoryName?.let { repoName -> repositories.find { it.name == repoName } }

                Dependency(
                    name = name,
                    groupId = values["groupId"],
                    artifactId = values["artifactId"] ?: name,
                    version = Version(versionKey, version),
                    default = values.getOrElse("default", false),
                    category = values.getEnumOrElse("category", Category.Other),
                    scope = values.getEnumOrElse("scope", Scope.Compile),
                    logger = values["logger"],
                    repository = repo,
                )
            }

        val inputs = cfg.configMap("inputs")
            .map { (name, values) ->
                Input(name, values["display"], values["default"])
            }

        val features = cfg.configMap("features")
            .map { (name, values) ->
                Feature(name, values["default"] ?: false)
            }

        return StarterConfig(dependencies, inputs, features)
    }
}
