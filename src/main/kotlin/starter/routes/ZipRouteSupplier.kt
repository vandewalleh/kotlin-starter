package starter.routes

import org.http4k.core.Method
import org.http4k.core.Request
import org.http4k.core.Response
import org.http4k.core.with
import org.http4k.routing.bind
import starter.ProjectZip
import starter.extensions.attachment
import starter.extensions.badRequest
import starter.extensions.ok
import starter.utils.ProjectExtractor
import java.io.ByteArrayInputStream

class ZipRouteSupplier(
    private val projectExtractor: ProjectExtractor,
    private val projectZip: ProjectZip,
) : RouteSupplier {

    private fun handle(req: Request): Response {
        val project = projectExtractor(req) ?: return Response.badRequest()

        val outputStream = projectZip.createZip(project)

        return Response.ok().with(
            attachment(
                value = ByteArrayInputStream(outputStream.toByteArray()),
                name = "${project.name}.zip",
                contentType = "application/zip"
            )
        )
    }

    override fun get() = "/" bind Method.POST to ::handle
}
