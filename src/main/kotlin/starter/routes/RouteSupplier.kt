package starter.routes

import org.http4k.routing.RoutingHttpHandler
import org.http4k.routing.routes

interface RouteSupplier {
    fun get(): RoutingHttpHandler
}

fun List<RouteSupplier>.toRouter() = routes(*map { it.get() }.toTypedArray())
