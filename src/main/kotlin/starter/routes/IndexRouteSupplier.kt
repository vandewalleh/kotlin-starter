package starter.routes

import org.http4k.core.Method
import org.http4k.core.Response
import org.http4k.core.Status
import org.http4k.routing.bind
import starter.views.Views

class IndexRouteSupplier(private val views: Views) : RouteSupplier {
    override fun get() = "/" bind Method.GET to {
        Response(Status.OK)
            .body(views.index())
            .header("Content-Type", "text/html")
    }
}
