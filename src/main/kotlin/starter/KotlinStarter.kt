package starter

import org.koin.core.context.startKoin
import org.koin.core.context.unloadKoinModules
import starter.modules.*

fun main() {
    startKoin {
        modules(mainModule, pebbleModule, templateModule, configModule, routesModule)
    }
    unloadKoinModules(configModule)
}
