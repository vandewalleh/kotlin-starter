package starter.templates

import com.mitchellbosecke.pebble.PebbleEngine
import starter.Project
import starter.utils.render

class EditorConfigTemplate(private val engine: PebbleEngine) : Template {
    override fun path(project: Project) =
        ".editorconfig"

    override fun enabled(project: Project) = project.features.any { it.name == "ktlint" }

    override fun render(project: Project) =
        engine.render("starter/editorconfig/index")
}
