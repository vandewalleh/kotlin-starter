package starter.templates

import com.mitchellbosecke.pebble.PebbleEngine
import starter.Project
import starter.utils.prettyPrintXml
import starter.utils.render

class PomTemplate(private val engine: PebbleEngine) : Template {
    override fun path(project: Project) = "pom.xml"

    override fun render(project: Project): String {
        val args: MutableMap<String, Any?> = mutableMapOf(
            "dependencies" to project.dependencies.sortedBy { it.scope },
            "repositories" to project.repositories,
            "kotlinxSerialization" to project.dependencies.any { it.name == "kotlinx-serialization" },
            "features" to project.features,
            "versions" to project.dependencies.map { it.version }.toSet()
        )

        project.inputs.forEach {
            args[it.name] = it.value
        }

        val rendered = engine.render("starter/pom/index", args)
        return prettyPrintXml(rendered)
    }
}
