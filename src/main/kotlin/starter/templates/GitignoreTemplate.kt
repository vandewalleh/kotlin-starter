package starter.templates

import com.mitchellbosecke.pebble.PebbleEngine
import starter.Project
import starter.utils.render

class GitignoreTemplate(private val engine: PebbleEngine) : Template {
    override fun path(project: Project) =
        ".gitignore"

    override fun render(project: Project) =
        engine.render("starter/gitignore/index")
}
