package starter.templates

import com.mitchellbosecke.pebble.PebbleEngine
import starter.Project
import starter.utils.render

class MainTemplate(private val engine: PebbleEngine) : Template {
    override fun path(project: Project) =
        "src/main/kotlin/" + project.name.toLowerCase().capitalize() + ".kt"

    override fun render(project: Project) =
        engine.render("starter/main/index", mapOf("basePackage" to project.basePackage))
}
