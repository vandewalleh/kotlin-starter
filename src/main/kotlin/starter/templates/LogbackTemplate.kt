package starter.templates

import com.mitchellbosecke.pebble.PebbleEngine
import starter.Project
import starter.utils.prettyPrintXml
import starter.utils.render

class LogbackTemplate(private val engine: PebbleEngine) : Template {
    override fun path(project: Project) = "src/main/resources/logback.xml"

    override fun enabled(project: Project) = project.dependencies.any { it.name == "logback" }

    override fun render(project: Project): String {
        val args = mapOf(
            "loggers" to project.dependencies.mapNotNull { it.logger }.toSet()
        )
        val rendered = engine.render("starter/logback/index", args)
        return prettyPrintXml(rendered)
    }
}
