package starter.templates

import com.mitchellbosecke.pebble.PebbleEngine
import starter.Project
import starter.utils.render

class JunitTemplate(private val engine: PebbleEngine) : Template {
    override fun path(project: Project) = "src/test/resources/junit-platform.properties"
    override fun enabled(project: Project) = project.dependencies.any { it.name == "junit" }
    override fun render(project: Project) = engine.render("starter/junit/index")
}
