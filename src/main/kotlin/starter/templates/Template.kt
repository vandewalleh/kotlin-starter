package starter.templates

import starter.Project

interface Template {
    fun path(project: Project): String
    fun enabled(project: Project): Boolean = true
    fun render(project: Project): String
}
