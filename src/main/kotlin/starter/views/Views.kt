package starter.views

import com.mitchellbosecke.pebble.PebbleEngine
import starter.config.StarterConfig
import starter.utils.render

class Views(private val engine: PebbleEngine, config: StarterConfig) {
    private val args = mapOf(
        "dependencies" to config.dependencies.groupBy { it.category }.toSortedMap(),
        "inputs" to config.inputs,
        "features" to config.features,
    )

    fun index() = engine.render("views/index", args)
}
