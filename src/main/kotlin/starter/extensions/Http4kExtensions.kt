@file:Suppress("NOTHING_TO_INLINE")

package starter.extensions

import org.http4k.core.Response
import org.http4k.core.Status
import starter.utils.sanitizeFilename
import java.io.InputStream

inline fun Response.Companion.ok() = Response(Status.OK)
inline fun Response.Companion.badRequest() = Response(Status.BAD_REQUEST)

fun attachment(value: InputStream, name: String, contentType: String) = { res: Response ->
    res.header("Content-Type", contentType)
        .header("Content-Disposition", "attachment; filename=\"${sanitizeFilename(name)}\"")
        .body(value)
}
