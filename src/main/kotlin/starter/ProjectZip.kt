package starter

import starter.templates.Template
import starter.utils.ZipOutput
import starter.utils.sanitizeFilename
import java.io.ByteArrayOutputStream

class ProjectZip(private val templates: List<Template>) {

    fun createZip(project: Project): ByteArrayOutputStream {
        val projectName = sanitizeFilename(project.name)
        val zipOutput = ZipOutput()
        zipOutput.use { zip ->
            templates.filter { it.enabled(project) }.forEach { template ->
                zip.write(projectName + "/" + template.path(project), template.render(project))
            }
        }
        return zipOutput.outputStream
    }
}
