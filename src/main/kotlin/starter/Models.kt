package starter

enum class Category {
    Http4k, Injection, Database, Serialization, Test, Other
}

enum class Scope {
    Compile, Test
}

data class Dependency(
    val name: String,
    val groupId: String,
    val artifactId: String,
    val version: Version,
    val default: Boolean,
    val category: Category,
    val scope: Scope,
    val logger: String?,
    val repository: Repository?,
)

data class Repository(val name: String, val url: String)

data class Input(val name: String, val display: String, val value: String? = null)

data class Feature(val name: String, val value: Boolean = false)

data class Project(
    val name: String,
    val basePackage: String,
    val inputs: List<Input>,
    val features: List<Feature>,
    val dependencies: List<Dependency>,
    val repositories: Set<Repository>,
)

data class Version(val name: String, val value: String)
