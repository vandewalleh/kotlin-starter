package starter.modules

import org.koin.dsl.bind
import org.koin.dsl.module
import starter.templates.*

val templateModule = module {
    single { PomTemplate(get()) } bind Template::class
    single { MainTemplate(get()) } bind Template::class
    single { LogbackTemplate(get()) } bind Template::class
    single { GitignoreTemplate(get()) } bind Template::class
    single { JunitTemplate(get()) } bind Template::class
    single { EditorConfigTemplate(get()) } bind Template::class
}
