package starter.modules

import org.koin.dsl.bind
import org.koin.dsl.module
import starter.pebble.DepAsXmlPebbleFunction
import starter.pebble.HasFeatureFilter
import starter.utils.PebbleEngineBuilder
import starter.utils.PebbleEngineBuilder.CacheType.ConcurrentMap
import starter.utils.PebbleFilter
import starter.utils.PebbleFunction

val pebbleModule = module {
    single { DepAsXmlPebbleFunction() } bind PebbleFunction::class
    single { HasFeatureFilter() } bind PebbleFilter::class

    single {
        PebbleEngineBuilder {
            cache = ConcurrentMap
            functions(getAll())
            filters(getAll())
            classPath { suffix = ".twig" }
        }
    }
}
