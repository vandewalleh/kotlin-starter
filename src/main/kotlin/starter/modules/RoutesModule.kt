package starter.modules

import org.http4k.core.then
import org.http4k.filter.ServerFilters
import org.http4k.routing.ResourceLoader
import org.http4k.routing.routes
import org.http4k.routing.static
import org.koin.dsl.bind
import org.koin.dsl.module
import starter.routes.IndexRouteSupplier
import starter.routes.RouteSupplier
import starter.routes.ZipRouteSupplier
import starter.routes.toRouter
import starter.utils.ProjectExtractor
import starter.utils.ProjectExtractorImpl

val routesModule = module {
    single { IndexRouteSupplier(get()) } bind RouteSupplier::class
    single { ZipRouteSupplier(get(), get()) } bind RouteSupplier::class
    single<ProjectExtractor> { ProjectExtractorImpl(get()) }
    single {
        ServerFilters.CatchAll().then(
            routes(
                static(ResourceLoader.Classpath("/assets")),
                getAll<RouteSupplier>().toRouter()
            )
        )
    }
}
