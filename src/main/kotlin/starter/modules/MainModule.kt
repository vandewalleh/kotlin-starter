package starter.modules

import org.http4k.routing.RoutingHttpHandler
import org.http4k.server.SunHttp
import org.http4k.server.asServer
import org.koin.dsl.module
import org.koin.dsl.onClose
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import starter.ProjectZip
import starter.views.Views

val mainModule = module {
    single(createdAtStart = true) {
        get<Logger>().info("Starting on http://localhost:7000")
        get<RoutingHttpHandler>().asServer(SunHttp(7000)).start()
    } onClose { it?.stop() }

    single { LoggerFactory.getLogger("Starter") }
    single { Views(get(), get()) }
    single { ProjectZip(getAll()) }
}
