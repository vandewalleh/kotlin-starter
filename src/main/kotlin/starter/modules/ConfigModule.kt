package starter.modules

import com.electronwill.nightconfig.core.file.FileConfig
import org.koin.dsl.module
import starter.config.Config
import com.electronwill.nightconfig.core.Config as NightConfig

val configModule = module {
    single<NightConfig> { FileConfig.of("config.toml").apply { load() } }
    single { Config(get()).load() }
}
