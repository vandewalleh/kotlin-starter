package starter.utils

import org.apache.commons.compress.archivers.zip.ZipArchiveEntry
import org.apache.commons.compress.archivers.zip.ZipArchiveOutputStream
import java.io.ByteArrayOutputStream

class ZipOutput : AutoCloseable {
    val outputStream = ByteArrayOutputStream()
    private val zipOutputStream = ZipArchiveOutputStream(outputStream)

    fun write(path: String, content: String) {
        val entry = ZipArchiveEntry(path)
        zipOutputStream.putArchiveEntry(entry)
        zipOutputStream.write(content.toByteArray())
        zipOutputStream.closeArchiveEntry()
    }

    override fun close() {
        zipOutputStream.finish()
        zipOutputStream.close()
    }
}
