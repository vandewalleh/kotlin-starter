package starter.utils

import org.http4k.core.Request
import org.http4k.core.body.form
import org.http4k.core.body.formAsMap
import starter.Project
import starter.config.StarterConfig

interface ProjectExtractor {
    operator fun invoke(request: Request): Project?
}

class ProjectExtractorImpl(private val conf: StarterConfig) : ProjectExtractor {
    override fun invoke(request: Request): Project? {
        val deps = conf.dependencies.filter {
            request.form(it.name) != null
        }

        val formMap = request.formAsMap()

        val inputKeys = conf.inputs.map { it.name }
        val inputs = formMap
            .filter { it.key in inputKeys }
            .map { (name, value) ->
                conf.inputs.find { it.name == name }!!.copy(value = value.first())
            }

        val features = formMap
            .filter { it.key in conf.features.map { it.name } }
            .map { (name, _) ->
                conf.features.find { it.name == name }!!.copy(value = true)
            }

        val projectName = inputs.find { it.name == "name" }?.value ?: return null
        val basePackage = inputs.find { it.name == "basePackage" }?.value ?: return null

        return if (basePackage.contains("/") || basePackage.contains("..")) {
            null
        } else {
            val repositories = deps.mapNotNull { it.repository }.toSet()
            Project(projectName, basePackage, inputs, features, deps, repositories)
        }
    }
}
