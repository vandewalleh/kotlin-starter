package starter.utils

import org.assertj.core.api.Assertions.assertThat
import org.http4k.core.Method
import org.http4k.core.Request
import org.http4k.core.body.Form
import org.http4k.core.body.toBody
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.MethodSource
import org.koin.dsl.koinApplication
import org.koin.dsl.module
import starter.Feature
import starter.Input
import starter.Project
import starter.modules.routesModule
import starter.testConfig
import java.util.stream.Stream

internal class ProjectExtractorImplTest {

    private val fakeModule = module {
        single { testConfig }
    }

    private val koin = koinApplication {
        modules(routesModule, fakeModule)
    }.koin

    private val projectExtractor = koin.get<ProjectExtractor>()

    @Suppress("unused")
    fun invalidProjectsSource(): Stream<Form> = Stream.of(
        emptyList(),
        listOf(
            "basePackage" to "org.example",
        ),
        listOf(
            "basePackage" to "org.example/a",
            "name" to "test"
        ),
    )

    @ParameterizedTest
    @MethodSource("invalidProjectsSource")
    fun invalidProjects(form: Form) {
        val request = Request(Method.POST, "")
            .body(form.toBody())

        val project = projectExtractor(request)

        assertThat(project).isNull()
    }

    @Suppress("unused")
    fun validProjectsSource(): Stream<Pair<Form, Project>> = Stream.of(
        listOf(
            "name" to "Test",
            "basePackage" to "org.example",
            "ktlint" to "",
        ) to Project(
            name = "Test",
            basePackage = "org.example",
            inputs = listOf(
                Input(name = "name", display = "name", value = "Test"),
                Input(name = "basePackage", display = "Base package", value = "org.example"),
            ),
            features = listOf(
                Feature(name = "ktlint", value = true)
            ),
            dependencies = listOf(),
            repositories = setOf(),
        ),
        listOf(
            "name" to "Test",
            "basePackage" to "org.example",
            "ktlint" to "",
            "koin" to "",
        ) to Project(
            name = "Test",
            basePackage = "org.example",
            inputs = listOf(
                Input(name = "name", display = "name", value = "Test"),
                Input(name = "basePackage", display = "Base package", value = "org.example"),
            ),
            features = listOf(
                Feature(name = "ktlint", value = true)
            ),
            dependencies = listOf(
                testConfig.dependencies.find { it.name == "koin" }!!
            ),
            repositories = setOf(
                testConfig.dependencies.find { it.name == "koin" }!!.repository!!
            ),
        ),
    )

    @ParameterizedTest
    @MethodSource("validProjectsSource")
    fun validProjects(pair: Pair<Form, Project>) {
        val (form, expected) = pair

        val request = Request(Method.POST, "")
            .body(form.toBody())

        val project = projectExtractor(request)

        assertThat(project).isEqualTo(expected)
    }
}
