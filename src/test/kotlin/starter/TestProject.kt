package starter

import starter.config.StarterConfig

val testConfig = StarterConfig(
    dependencies = listOf(
        Dependency(
            name = "h2",
            groupId = "com.h2database",
            artifactId = "h2",
            version = Version("h2", "1.4.200"),
            default = false,
            category = Category.Database,
            scope = Scope.Compile,
            logger = null,
            repository = null,
        ),
        Dependency(
            name = "assertj",
            groupId = "org.assertj",
            artifactId = "assertj-core",
            version = Version("assertj", "3.17.2"),
            default = true,
            category = Category.Test,
            scope = Scope.Test,
            logger = null,
            repository = null,
        ),
        Dependency(
            name = "koin",
            groupId = "org.koin",
            artifactId = "koin-core",
            version = Version("koin", "2.1.6"),
            default = true,
            category = Category.Injection,
            scope = Scope.Compile,
            logger = null,
            repository = Repository("jcenter", "https://jcenter.bintray.com"),
        ),
    ),
    inputs = listOf(
        Input(name = "name", display = "name", value = "example"),
        Input(name = "basePackage", display = "Base package", value = "org.example"),
    ),
    features = listOf(Feature("ktlint", true))
)
