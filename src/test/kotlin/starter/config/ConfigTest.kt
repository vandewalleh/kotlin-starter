package starter.config

import com.electronwill.nightconfig.toml.TomlParser
import org.assertj.core.api.Assertions.assertThat
import org.intellij.lang.annotations.Language
import org.junit.jupiter.api.Test
import starter.*
import java.io.StringReader

internal class ConfigTest {
    private val parser = TomlParser()
    private fun parse(config: String) = parser.parse(StringReader(config))

    @Test
    fun inputs() {
        @Language("toml")
        val toml = """
            [inputs]

            [inputs.name]
            default = "example"
            display = "Project Name"

            [inputs.basePackage]
            default = "org.example"
            display = "Base package"
        """.trimIndent()

        val starterConfig = Config(parse(toml)).load()

        val expected = listOf(
            Input("basePackage", "Base package", "org.example"),
            Input("name", "Project Name", "example"),
        )

        assertThat(starterConfig.inputs).containsExactlyInAnyOrderElementsOf(expected)
    }

    @Test
    fun dependencies() {
        @Language("toml")
        val toml = """
            [versions]
            assertj = "3.17.2"
            koin = "2.1.6"
            h2 = "1.4.200"

            [repositories]

            [repositories.jcenter]
            url = "https://jcenter.bintray.com"

            [dependencies]

            [dependencies.assertj]
            groupId = "org.assertj"
            artifactId = "assertj-core"
            scope = "test"
            category = "test"
            default = true

            [dependencies.koin]
            groupId = "org.koin"
            artifactId = "koin-core"
            category = "injection"
            repository = "jcenter"
            default = true

            [dependencies.h2]
            groupId = "com.h2database"
            category = "database"
        """.trimIndent()

        val starterConfig = Config(parse(toml)).load()

        val expected = listOf(
            Dependency(
                name = "h2",
                groupId = "com.h2database",
                artifactId = "h2",
                version = Version("h2", "1.4.200"),
                default = false,
                category = Category.Database,
                scope = Scope.Compile,
                logger = null,
                repository = null,
            ),
            Dependency(
                name = "assertj",
                groupId = "org.assertj",
                artifactId = "assertj-core",
                version = Version("assertj", "3.17.2"),
                default = true,
                category = Category.Test,
                scope = Scope.Test,
                logger = null,
                repository = null,
            ),
            Dependency(
                name = "koin",
                groupId = "org.koin",
                artifactId = "koin-core",
                version = Version("koin", "2.1.6"),
                default = true,
                category = Category.Injection,
                scope = Scope.Compile,
                logger = null,
                repository = Repository("jcenter", "https://jcenter.bintray.com"),
            ),
        )

        assertThat(starterConfig.dependencies).containsExactlyInAnyOrderElementsOf(expected)
    }
}
